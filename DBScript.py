#DOOMBRINGER
#DOMBRINGE
def main():
	print("Input word")
	word = input()
	
	check(word.lower())
	
def check(word):
	chars = set("dombringe ")
	if all((c in chars) for c in word):
		print("It's good")
		change(word)
	else:
		print("Borke")
		main()
		
def change(word):
	sentence = ""
	for c in word:
		if c == "d":
			sentence += ":db_d: "
		if c == "o":
			sentence += ":db_o: "
		if c == "m":
			sentence += ":db_m: "
		if c == "b":
			sentence += ":db_b: "
		if c == "r":
			sentence += ":db_r: "
		if c == "i":
			sentence += ":db_i: "
		if c == "n":
			sentence += ":db_n: "
		if c == "g":
			sentence += ":db_g: "
		if c == "e":
			sentence += ":db_e: "
		if c == " ":
			sentence += "   "
	
	print(sentence)

main()
